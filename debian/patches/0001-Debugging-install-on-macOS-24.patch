From 98b00b879b0e10202e6ebb4442d8ae5091b73858 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?H=C3=A5kon=20H=C3=A6gland?= <hakon.hagland@gmail.com>
Date: Tue, 9 Jul 2024 21:34:06 +0200
Subject: [PATCH] Debugging install on macOS (#24)

Bug-Debian: https://bugs.debian.org/1077194
Bug: https://github.com/hakonhagland/perl-math-gsl/issues/22

diff --git a/lib/Math/GSL/Test.pm b/lib/Math/GSL/Test.pm
index a44fa57..ab97f00 100644
--- a/lib/Math/GSL/Test.pm
+++ b/lib/Math/GSL/Test.pm
@@ -42,6 +42,41 @@ sub _dump_result($)
     diag sprintf( "result->val: %.18g\n", $r->{val});
 }
 
+sub dump_lu_matrix
+{
+    # Syntax: dump_lu_matrix($base->raw, $permutation, 4, 4);
+
+    my ($base, $perm, $m, $n) = @_;
+    # Dump the upper triangular matrix first:
+    my $U = "U = [";
+    for my $i (0..$m-1) {
+        for my $j (0..$n-1) {
+            if ($j >= $i) {
+                $U .= sprintf "%g ", $base->[$i*$n + $j];
+            } else {
+                $U .= "0 ";
+            }
+        }
+        $U .= "\n";
+    }
+    diag $U . "]\n";
+    # Now dump the lower triangular matrix:
+    my $L = "L = [";
+    for my $i (0..$m-1) {
+        for my $j (0..$n-1) {
+            if ($j < $i) {
+                $L .= sprintf "%g ", $base->[$i*$n + $j];
+            } elsif ($j == $i) {
+                $L .= "1 ";
+            } else {
+                $L .= "0 ";
+            }
+        }
+        $L .= "\n";
+    }
+    diag $L . "]\n";
+}
+
 =head2 is_windows()
 
 Returns true if current system is Windows-like.
diff --git a/t/Linalg.t b/t/Linalg.t
index 5303aff..bd20c6b 100644
--- a/t/Linalg.t
+++ b/t/Linalg.t
@@ -44,7 +44,10 @@ sub GSL_LINALG_LU_DECOMP : Tests {
     $base->set_row(0, [0,1,2,3])
          ->set_row(1, [5,6,7,8])
          ->set_row(2, [9,10,11,12])
-         ->set_row(3, [13,14,15,16]);
+         # NOTE: Using [13,14,15,16] here causes a singular matrix but due to 
+         #     rounding errors it is not recognized as such on some platforms,
+         #    so we will instead repeat the third row to make it singular in a more obvious way
+         ->set_row(3, [9,10,11,12]);
     my $permutation = gsl_permutation_alloc(4);
     gsl_permutation_init($permutation);
     my $first = Math::GSL::Matrix->new(4,4);
@@ -53,16 +56,12 @@ sub GSL_LINALG_LU_DECOMP : Tests {
     my ($result, $signum) = gsl_linalg_LU_decomp($base->raw, $permutation);
     my $version= gsl_version();
     my ($major, $minor) = split /\./, $version;
+    #Math::GSL::Test::dump_lu_matrix([$base->as_list], $permutation, 4, 4);
     if ($major >= 2 && $minor >= 8) {
         # From version 2.8 onwards, the result value for a singular matrix is an integer
         #  such that U(i,i) = 0
         # TODO: For some reason this is not the case on macOS yet
-        if ($^O eq 'darwin') {
-            is_deeply( [ $result, $signum ], [ 0, 1] );
-        }
-        else {
-            is_deeply( [ $result, $signum ], [ 4, 1] );
-        }
+        is_deeply( [ $result, $signum ], [ 4, 1] );
     }
     else {
         is_deeply( [ $result, $signum ], [ 0, 1] );
